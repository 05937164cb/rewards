const stylesheet = "/css/wows_numbers.css";
const root = __dirname;
const layoutsDir = root+"/public";
import "reflect-metadata";
import { db } from "./db";
import { getConnection } from "typeorm";
import { users } from "./entity/users";
import { types } from "./entity/types";
import { camouflages } from "./entity/camouflages";
import { consumables } from "./entity/consumables";
import { currency } from "./entity/currency";
import { flags } from "./entity/flags";
import { icons } from "./entity/icons";
import { specials } from "./entity/specials";
import { auth0 } from "jsonwebtoken"

module.exports = async function (app) {
  process.env.NODE_ENV = process.env.NODE_ENV || "development";
  const conn = getConnection(process.env.NODE_ENV);

  const webAuth = new auth0.webAuth({
    domain: "8eb9141e41.eu.auth0.com",
    client_id: "UzJvd-8jdg4_SzccuyIfcziguwdYoXlm",
    callback_url: "https://api.wows.rule10.eu/api/v1/cb",
    audience: "https://8eb9141e41.eu.auth0.com/api/v2/"
  });

  app.get('/', function(req, res) {
      res.send("Welcome to root");
    });

    app.get("/api/v1/cb",function(req,res){
      // Do authentication stuff
    });
    
    app.get("/api/v1/user/:uid",async function(req,res){
      var id = req.params.uid;
      try{
        let user = await conn.getRepository(users).findOneOrFail({id: id});
        let camouflage = await conn.getRepository(camouflages).find({uid: id});
        let consumable = await conn.getRepository(consumables).find({uid:id});
        let currencies = await conn.getRepository(currency).find({uid:id});
        let flag = await conn.getRepository(flags).find({uid:id});
        let special = await conn.getRepository(specials).find({uid:id});
        res.json({camouflageCount: camouflage.length, consumableCount: consumable.length, currencyCount: currencies.length, flagCount: flag.length, specialCount: special.length, user: user.nickname});
      }catch(e){
        throw new Error("ERROR ::\n\n"+e);
      }
    });
    
    app.get("/api/v1/user/:uid/:super/:type",async function(req,res){
      // super -> 1 = super, 0 = normal
      // let result = usersRepo.findOneOrFail({id:req.params.uid});
      try{
        let table = await conn.getRepository(types).findOneOrFail({id:req.params.type});
        let result = await conn.getRepository(table.name)
                .find({type: req.params.super, uid: req.params.uid});
        let asd = table.name+".icon";
        let ress = await conn.getRepository(table.name)
                  .createQueryBuilder(table.name)
                  .leftJoinAndMapMany(asd,icons,"icons","icons.id="+asd)
                  .getMany();
        let usr = await conn.getRepository(users).findOneOrFail({id: req.params.uid});
        let allIcons = await conn.getRepository(icons).find();
        // res.render(layoutsDir + "/view.hbs",{layout: "view.hbs",uid:usr.nickname, super: req.params.super, type:table.name, stylesheet:stylesheet, result: result, icons: allIcons});
        res.json({uid: usr.nickname, super: req.params.super, type: table.name, result: result});
      }catch(e){
        throw new Error("ERROR ::\n\n"+e);
      }
    });

    app.post("/api/v1/login",async function(req,res){
      try{

      }catch(e){
        throw new Error("ERROR ::\n\n"+e);
      }
    });
    
    // user modifications
    
    app.get("/api/v1/user/:uid/create/:type",function(req,res){
      res.render(layoutsDir + "/create.hbs",{layout: "create.hbs", uid: req.params.uid, type: req.params.type, stylesheet: stylesheet});
    });
    
    app.get("/api/v1/user/:uid/add/:id",function(req,res){
      res.render(layoutsDir + "/add.hbs",{layout: "add.hbs", uid: req.params.uid, id: req.params.id, stylesheet: stylesheet});
    });
  }