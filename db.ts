import { getConnection } from "typeorm";
import { users } from "./entity/users";
process.env.NODE_ENV = process.env.NODE_ENV || "development";

export class db{
    getConn(){
        return getConnection(process.env.NODE_ENV);
    }

    public usersRepo(){
        return this.getConn().getRepository(users);
    }
}

export class repos extends db{
    // usersRepo(){
    //   return this.getConn().getRepository(users);
    // }
}