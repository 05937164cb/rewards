var express = require('express');
var app = express();
var path = require("path");
var port = 8080;
var hbs = require('express-handlebars');
var root = __dirname;
import { createConnection, ConnectionOptions } from "typeorm";
import "reflect-metadata";

async function init(){
  process.env.NODE_ENV = process.env.NODE_ENV || "development";
  await createConnection(process.env.NODE_ENV).then(
    connection=>{}
    ).catch(error=>console.log(error) )

  await app.listen(port);
  console.log('server is running on port '+port);

  app.set('views', path.join(root, '/public'));
  app.engine("hbs",hbs(
                        {
                          extname:"hbs",
                          defaultLayout: "layout",
                          layoutsDir: root+"/public"
                        }
                      )
            );
  app.set('view engine', 'hbs');
  app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
              "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  require("./routes")(app);
}
init();