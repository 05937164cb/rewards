import {Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn} from "typeorm";

@Entity()
export class camouflages {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default: 0})
    amount: number;

    @Column({default: 0})
    type: number;

    @Column({default: 0})
    box_type: number;

    @Column({default: 0})
    icon: string;

    @Column()
    uid: number;

    @UpdateDateColumn()
    date: Date;

}