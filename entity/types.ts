import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class types {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({default: "0"})
    name: string;
}