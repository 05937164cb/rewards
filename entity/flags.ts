import {Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn} from "typeorm";

@Entity()
export class flags {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default: 0})
    amount: number;

    @Column({default: 0})
    type: number;

    // 0 = luck, 1 = currency, 2 = consumable, 3 = flags
    @Column({default: 0})
    box_type: number;

    @Column()
    icon: string;

    @Column()
    uid: number;

    @UpdateDateColumn()
    date: Date;
}