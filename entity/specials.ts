import {Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn} from "typeorm";

@Entity()
export class specials {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default: 0})
    amount: number;

    @Column({default: 0})
    type: number

    @Column({default: 0})
    boxtype: number;

    @Column({default: 0})
    icon: string;

    @Column({default: 0})
    uid: number;

    @UpdateDateColumn()
    date: Date;
}