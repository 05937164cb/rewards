import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class users {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    usr: string;

    @Column()
    pwd: string;

    @Column()
    salt: string;

    @Column()
    nickname: string;

    @Column()
    email: string;

    @Column()
    key: string;
}
